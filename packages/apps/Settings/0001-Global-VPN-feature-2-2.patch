From 16c9311e41992ddd8d0bfb5a340cedbf001e3413 Mon Sep 17 00:00:00 2001
From: Oliver Scott <olivercscott@gmail.com>
Date: Thu, 8 Jul 2021 10:40:49 -0400
Subject: [PATCH] Global VPN feature [2/2]

* Create a global VPN toggle for VPNs in the system user. It is only
  enabled when no VPN is active in any user.

Change-Id: Ic3b79beb635afe03642fce9473bc481239166566
Signed-off-by: Mohammad Hasan Keramat J <ikeramat@protonmail.com>
---
 res/values/strings.xml                        |  5 ++
 res/xml/vpn_app_management.xml                |  6 +++
 .../settings/vpn2/AppManagementFragment.java  | 48 ++++++++++++++++++-
 3 files changed, 58 insertions(+), 1 deletion(-)

diff --git a/res/values/strings.xml b/res/values/strings.xml
index 28b35b3fcf..29ca3882e9 100644
--- a/res/values/strings.xml
+++ b/res/values/strings.xml
@@ -14457,4 +14457,9 @@
     <!-- [CHAR LIMIT=NONE] Hint for QR code process failure -->
     <string name="bt_le_audio_qr_code_is_not_valid_format">QR code isn\u0027t a valid format</string>
 
+    <!-- VPN app management screen, global VPN -->
+    <string name="global_vpn_title">Global VPN</string>
+    <string name="global_vpn_summary">Force all traffic on the device through this VPN, including work profile and other users.</string>
+    <string name="global_vpn_summary_on">Force all traffic on the device through this VPN, including work profile and other users. Note: When enabled, you will not be able to use a separate VPN in a work profile or other users</string>
+    <string name="global_vpn_summary_any_vpn_active">You need to disable all active VPN connections first to enable this</string>
 </resources>
diff --git a/res/xml/vpn_app_management.xml b/res/xml/vpn_app_management.xml
index adc441d846..e00f23ccfa 100644
--- a/res/xml/vpn_app_management.xml
+++ b/res/xml/vpn_app_management.xml
@@ -31,6 +31,12 @@
                 android:selectable="false"/>
         -->
 
+        <SwitchPreference
+                android:key="global_vpn"
+                android:title="@string/global_vpn_title"
+                android:defaultValue="false"
+                android:summary="@string/global_vpn_summary" />
+
         <com.android.settingslib.RestrictedSwitchPreference
                 android:order="10"
                 android:key="always_on_vpn"
diff --git a/src/com/android/settings/vpn2/AppManagementFragment.java b/src/com/android/settings/vpn2/AppManagementFragment.java
index d4ee5b9c47..7a52e0c42c 100644
--- a/src/com/android/settings/vpn2/AppManagementFragment.java
+++ b/src/com/android/settings/vpn2/AppManagementFragment.java
@@ -28,10 +28,12 @@ import android.content.pm.ApplicationInfo;
 import android.content.pm.PackageInfo;
 import android.content.pm.PackageManager;
 import android.content.pm.PackageManager.NameNotFoundException;
+import android.content.pm.UserInfo;
 import android.net.VpnManager;
 import android.os.Bundle;
 import android.os.UserHandle;
 import android.os.UserManager;
+import android.provider.Settings;
 import android.text.TextUtils;
 import android.util.Log;
 import android.widget.TextView;
@@ -41,6 +43,7 @@ import androidx.appcompat.app.AlertDialog;
 import androidx.fragment.app.DialogFragment;
 import androidx.preference.Preference;
 import androidx.preference.PreferenceViewHolder;
+import androidx.preference.SwitchPreference;
 
 import com.android.internal.net.VpnConfig;
 import com.android.internal.util.ArrayUtils;
@@ -64,6 +67,7 @@ public class AppManagementFragment extends SettingsPreferenceFragment
     private static final String ARG_PACKAGE_NAME = "package";
 
     private static final String KEY_VERSION = "version";
+    private static final String KEY_GLOBAL_VPN = "global_vpn";
     private static final String KEY_ALWAYS_ON_VPN = "always_on_vpn";
     private static final String KEY_LOCKDOWN_VPN = "lockdown_vpn";
     private static final String KEY_FORGET_VPN = "forget_vpn";
@@ -79,6 +83,7 @@ public class AppManagementFragment extends SettingsPreferenceFragment
     private String mVpnLabel;
 
     // UI preference
+    private SwitchPreference mPreferenceGlobal;
     private RestrictedSwitchPreference mPreferenceAlwaysOn;
     private RestrictedSwitchPreference mPreferenceLockdown;
     private RestrictedPreference mPreferenceForget;
@@ -123,10 +128,16 @@ public class AppManagementFragment extends SettingsPreferenceFragment
         mDevicePolicyManager = getContext().getSystemService(DevicePolicyManager.class);
         mVpnManager = getContext().getSystemService(VpnManager.class);
 
+        mPreferenceGlobal = (SwitchPreference) findPreference(KEY_GLOBAL_VPN);
         mPreferenceAlwaysOn = (RestrictedSwitchPreference) findPreference(KEY_ALWAYS_ON_VPN);
         mPreferenceLockdown = (RestrictedSwitchPreference) findPreference(KEY_LOCKDOWN_VPN);
         mPreferenceForget = (RestrictedPreference) findPreference(KEY_FORGET_VPN);
 
+        if (mUserId != UserHandle.USER_SYSTEM) {
+            removePreference(KEY_GLOBAL_VPN);
+        }
+
+        mPreferenceGlobal.setOnPreferenceChangeListener(this);
         mPreferenceAlwaysOn.setOnPreferenceChangeListener(this);
         mPreferenceLockdown.setOnPreferenceChangeListener(this);
         mPreferenceForget.setOnPreferenceClickListener(this);
@@ -204,6 +215,8 @@ public class AppManagementFragment extends SettingsPreferenceFragment
     @Override
     public boolean onPreferenceChange(Preference preference, Object newValue) {
         switch (preference.getKey()) {
+            case KEY_GLOBAL_VPN:
+                return onGlobalVpnClick((Boolean) newValue);
             case KEY_ALWAYS_ON_VPN:
                 return onAlwaysOnVpnClick((Boolean) newValue, mPreferenceLockdown.isChecked());
             case KEY_LOCKDOWN_VPN:
@@ -243,6 +256,11 @@ public class AppManagementFragment extends SettingsPreferenceFragment
         return setAlwaysOnVpnByUI(alwaysOnSetting, lockdown);
     }
 
+    private boolean onGlobalVpnClick(final boolean global) {
+        return Settings.Global.putString(getContext().getContentResolver(),
+                Settings.Global.GLOBAL_VPN_APP, global ? mPackageName : "");
+    }
+
     @Override
     public void onConfirmLockdown(Bundle options, boolean isEnabled, boolean isLockdown) {
         setAlwaysOnVpnByUI(isEnabled, isLockdown);
@@ -276,7 +294,18 @@ public class AppManagementFragment extends SettingsPreferenceFragment
             final boolean alwaysOn = isVpnAlwaysOn();
             final boolean lockdown = alwaysOn
                     && VpnUtils.isAnyLockdownActive(getActivity());
-
+            final boolean anyVpnActive = isAnyVpnActive();
+            final boolean globalVpn = isGlobalVpn();
+
+            mPreferenceGlobal.setEnabled(!anyVpnActive);
+            mPreferenceGlobal.setChecked(globalVpn);
+            if (globalVpn) {
+                mPreferenceGlobal.setSummary(R.string.global_vpn_summary_on);
+            } else if (anyVpnActive) {
+                mPreferenceGlobal.setSummary(R.string.global_vpn_summary_any_vpn_active);
+            } else {
+                mPreferenceGlobal.setSummary(R.string.global_vpn_summary);
+            }
             mPreferenceAlwaysOn.setChecked(alwaysOn);
             mPreferenceLockdown.setChecked(lockdown);
             updateRestrictedViews();
@@ -322,6 +351,11 @@ public class AppManagementFragment extends SettingsPreferenceFragment
         return mPackageName.equals(getAlwaysOnVpnPackage());
     }
 
+    private boolean isGlobalVpn() {
+        return mPackageName.equals(Settings.Global.getString(
+                getContext().getContentResolver(), Settings.Global.GLOBAL_VPN_APP));
+    }
+
     /**
      * @return false if the intent doesn't contain an existing package or can't retrieve activated
      * vpn info.
@@ -376,6 +410,18 @@ public class AppManagementFragment extends SettingsPreferenceFragment
         return config != null && !TextUtils.equals(config.user, mPackageName);
     }
 
+    /**
+     * @return {@code true} if any VPN (VpnService or legacy) is connected or set as always-on.
+     */
+    private boolean isAnyVpnActive() {
+        for (UserInfo userInfo : UserManager.get(getContext()).getUsers()) {
+            if (mVpnManager.getVpnConfig(userInfo.id) != null) {
+                return true;
+            }
+        }
+        return false;
+    }
+
     public static class CannotConnectFragment extends InstrumentedDialogFragment {
         private static final String TAG = "CannotConnect";
         private static final String ARG_VPN_LABEL = "label";
-- 
2.40.0

